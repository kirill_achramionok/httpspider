package by.achramionok;

import java.io.BufferedReader;
import java.util.Iterator;

public class Provider implements Iterator<String> {
    private BufferedReader reader;
    private String nextValue;
    private int counter = 0;
    private long numbersOfSites;

    Provider(BufferedReader reader, long numbersOfSites) {
        this.reader = reader;
        this.numbersOfSites = numbersOfSites;
    }

    @Override
    public boolean hasNext() {
        try {
            nextValue = reader.readLine();
            if (nextValue != null && counter != numbersOfSites) {
                counter++;
            } else {
                return false;
            }
            return nextValue != null;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public String next() {
        if (nextValue != null) {
            return "http://" + nextValue.split(",")[2];
        }
        return null;
    }

}
