package by.achramionok.spider;


import by.achramionok.Provider;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Spider extends Thread {
    private static Logger log = LoggerFactory.getLogger(Spider.class);
    private Pattern pattern;
    private final Provider provider;
    private CloseableHttpClient httpClient;
    private AtomicLong time = new AtomicLong(0);
    private AtomicLong operations = new AtomicLong(0);
    private AtomicLong success = new AtomicLong(0);
    private AtomicLong countMatches = new AtomicLong(0);

    public Spider(Pattern pattern, CloseableHttpClient httpClient, Provider provider) {
        this.pattern = pattern;
        this.provider = provider;
        this.httpClient = httpClient;
    }

    @Override
    public void run() {
        String domainUrl;
        while (true) {
            long start = System.currentTimeMillis();
            synchronized (provider) {
                if (provider.hasNext()) {
                    domainUrl = provider.next();
                } else {
                    break;
                }
            }
            countMatches.addAndGet(getWebSiteData(domainUrl));
            long finish = System.currentTimeMillis();

            operations.incrementAndGet();
            time.addAndGet(finish - start);

        }
    }

    private long getWebSiteData(String domainUrl) {
        String line;
        long count = 0;
        HttpGet httpget = new HttpGet(domainUrl);
        try (CloseableHttpResponse response = httpClient.execute(httpget)) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                success.incrementAndGet();
                try (BufferedReader rd = new BufferedReader(
                        new InputStreamReader(entity.getContent()))) {
                    while ((line = rd.readLine()) != null) {
                        count = count + findMatches(line);
                    }
                } catch (IOException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return count;
    }

    private long findMatches(String line) {
        Matcher matcher = pattern.matcher(line);
        long count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    public AtomicLong getTime() {
        return time;
    }

    public AtomicLong getOperations() {
        return operations;
    }

    public AtomicLong getSuccess() {
        return success;
    }

    public AtomicLong getCountMatches() {
        return countMatches;
    }
}
