package by.achramionok;

import by.achramionok.spider.Spider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class Main {
    private static Logger log = LoggerFactory.getLogger(Main.class);
    private static Spider[] spiders;

    public static void main(String[] args) {
        RequestConfig config = RequestConfig.custom().
                setConnectTimeout(5000).
                setConnectionRequestTimeout(5000).
                setSocketTimeout(5000).build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(100);
        CloseableHttpClient httpclient = HttpClientBuilder.create().setConnectionManager(connectionManager).
                disableCookieManagement().disableRedirectHandling().disableAutomaticRetries().
                setDefaultRequestConfig(config).build();

        BufferedReader reader = null;
        Timer timer = null;
        try {
            reader = new BufferedReader(new FileReader("D:\\majestic_million.csv"));

            timer = new Timer();
            ScheduledStatistic statistic = new ScheduledStatistic();
            timer.schedule(statistic, 5000, 5000);

            Provider provider = new Provider(reader, 5000);
            spiders = new Spider[100];
            for (int i = 0; i < 100; i++) {
                spiders[i] = new Spider(Pattern.compile("google.com"), httpclient, provider);
                spiders[i].start();
            }

            for (Spider spider : spiders) {
                spider.join();
            }


            System.out.println("Result: " + "\n" + statistic.getStatistic());

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            try {
                httpclient.close();
                connectionManager.close();
                if (timer != null)
                    timer.cancel();
                if (reader != null) reader.close();
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    public static class ScheduledStatistic extends TimerTask {
        @Override
        public void run() {
            System.out.println(getStatistic());
        }

        String getStatistic() {
            StringBuilder builder = new StringBuilder();
            long executionTime = 0;
            long operationsNumbers = 0;
            long operationSuccess = 0;
            long countMatches = 0;
            for (Spider spider : spiders) {
                executionTime = executionTime + spider.getTime().get();
                operationsNumbers = operationsNumbers + spider.getOperations().get();
                operationSuccess = operationSuccess + spider.getSuccess().get();
                countMatches = countMatches + spider.getCountMatches().get();
            }
            builder.append("Execution time(sec):").append((executionTime / 1000) / 60).append(", ");
            builder.append("Operations numbers:").append(operationsNumbers).append(", ");
            builder.append("Operations success:").append(operationSuccess).append(", ");
            builder.append("Numbers of matches:").append(countMatches).append('.');
            return builder.toString();
        }
    }

}